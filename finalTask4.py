#Qasim Nadeem, Aayush Karki

import nltk, re, math


from genericFunctions import displayAccuracy


# Features for NER classifier in Task 4. Uses lexical AND phrasal context AND history of previous NER tags to greedily assign features to
# target word in given sentence.
# This function takes a sentence (a 3-tuple list) and index for word in the sentence that it needs to return the features for.
def ne_features4(sentence,i,history):
    
    word = sentence[i][0]
    posTag = sentence[i][1]
    phraseType = sentence[i][2]

    features = {
                "prefix(4)": word.lower()[:6],
                "Word": word.lower(),
                "suffix(2)": word.lower()[-2:], 
                "posTag": posTag,
                }



    #add previous word's POS-tag AND NER-tag as features.
    if i == 0:
        features["prevPOStag"] = "<START>"
        features["prevNERtag"] = False
    else:
        features["prevPOStag"] = sentence[i-1][1]
        features["prevNERtag"] = history[i-1]!="O"
        
    #add next word's POS-tag AND NER-tag as features.
    if i == (len(sentence)-1):
        features["nextPOStag"] = "<END>"
    else:
        features["nextPOStag"] = sentence[i+1][1]

    # Getting the POS tags of word 2-words earlier and later, than the target word: 
    if i<=1:
        features["2WordsEarlierPOS"] = "<START>"
        features["2WordsEarlierNER"] = False
    else:
        features["2WordsEarlierPOS"] = sentence[i-2][1]
        features["2WordsEarlierNER"] = history[i-2]!="O"

    if i >= (len(sentence)- 2):
        features["2WordsAfterPOS"] = "<END>"
    else:
        features["2WordsAfterPOS"] = sentence[i+2][1]

    #if NNP keep the word, otherwise empty string.
    if posTag=='NNP' or posTag=='NNPS':
        features['NNPword']=word
    else:
        features['NNPword'] = ''
         
    
                      
    return features



#TASK 4

# Slightly different from writer() for Task 1; does the same job, but slightly different requirements
# Takes input file, classifies its tokens using the given classifier
# as argument, and writes to outputfile in correct format for evaluation later.
def writer4(TClassifier, inpfile, outfile):
    g=open(outfile,'w')
     
    Sents = [[]]
 
    with open(inpfile,'r') as File:# getting dev/test file.
            for line in File:
                vals = line.split() #should be 4 values on a line, or emtpy list if empty line.
                 
                if vals == []: #whitespace on line. New sentence starting.
                    Sents.append([])
                else:
                    Sents[-1].append(tuple(vals))
    File.close()
     
    for sent in Sents:
        history = [] #We will greedily fill history list and pass it to feature extractor,
            # as we go through the sentence and tag it.
        
        for i in range(len(sent)):
            word = sent[i][0]
            goldTag = sent[i][3]
            wordFeatures = ne_features4([(val[0],val[1],val[2]) for val in sent], 
                                        i, 
                                        history)

            predTag = TClassifier.classify(wordFeatures)

            # add to history the tag predicted so it can be used for the next word's
            #feature extraction
            history.append(predTag) 
 
            if predTag !='O': 
                predTag= ("I-"+ predTag[2:]) #changing B-x to I-x.
                 
            wline = word + " " + goldTag +  " " + predTag
            g.write(wline+'\n')

            
    g.close()


trainSents = [[]] # A list of lists. Each sublist represents a sentence in trainFile, and has a series of 4-tuples of form
                   # (word, POS tag, 3rd col, NE tag).

with open('eng.train','r') as trainFile:# getting train file.
        for line in trainFile:
            vals = line.split() #should be 4 values on a line, or emtpy list if empty line.
            
            if vals == []: #whitespace on line. New sentence starting.
                trainSents.append([])
            else:
                trainSents[-1].append(tuple(vals))         
trainFile.close()

#I now have entire trainSents as a list of lists (each sublist consists of tuples).
featuresets = []

for tagged_sent in trainSents: # iterating over the lists in trainSents.
    for i in range(len(tagged_sent)):
        history = [(val[3]) for val in tagged_sent[:i]] # the NER tags of the word before target word.

        #Pass feature extractor, the 3 tuple (word, POS tag, phrase tag) list for sentence + the index of word + the history
        #of past NER tags.
        wordFeatures = ne_features4([(val[0],val[1],val[2]) for val in tagged_sent] , 
                                    i, 
                                    history) #features of word at index i
        featuresets.append((wordFeatures,  tagged_sent[i][3])) # append word features, and it's NER tag as a 2-tuple
        
Task4Classifier = nltk.NaiveBayesClassifier.train(featuresets)


writer4(Task4Classifier, 'eng.dev', 'out4_dev.txt')
writer4(Task4Classifier, 'eng.test', 'out4_test.txt')



displayAccuracy("4")

print "done with Task 4." 
