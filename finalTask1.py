#Qasim Nadeem, Aayush Karki

import nltk, re, math


from genericFunctions import displayAccuracy



# Basic features needed for NER in Task 1. Takes in a tuple that has the word and it's POS tag,
# and returns a dictionary of it's features. The features include the word's 3 different sized suffixes,
# it's POS tag, and whether the first letter of the word is capital or not.
def ne_features1(wordTuple):
    word = wordTuple[0]
    posTag = wordTuple[1]


    
    features = {"prefix(4)": word.lower()[:4],
                "suffix(2)": word.lower()[-2:],
                 
                "suffix(5)": word.lower()[-5:], 
                "posTag": posTag}    

    return features



#EVALUATE TASK 1 CLASSIFIER:
#    
# Takes input file, classifies its tokens using the given classifier
# as argument, and writes to outputfile in correct format for evaluation later.

def writer(trainedClassifier, inpfile, outputfilename):
    testfile =open(inpfile,'r')
    g=open(outputfilename,'w')
     
    for tline in testfile:
        tline = tline.split()
        if tline==[]:
            continue
        else:
            word = tline[0]
            goldTag= tline[3]
            wordTuple = (word,tline[1],tline[2],goldTag)
             
            predTag = trainedClassifier.classify(ne_features1(wordTuple))
             
            if predTag !='O':
                predTag= ("I-"+ predTag[2:])
                 
            wline = word + " " + goldTag +  " " + predTag
            g.write(wline+'\n')
 
    g.close()
    testfile.close()




trainTokens = [] # A list of tuples of form (word, POS tag, NE tag) to be sent to classifier

with open('eng.train','r') as trainFile:# getting train file.
        for line in trainFile:
                vals = line.split() #should be the 4 values on a line.
                if vals != []: # if line wasn't just whitespace.       
                    trainTokens.append(tuple(vals))
                    
trainFile.close()
#I now have the entire trainFile as a list of tuples. Each tuple has a word, its POS tag, 3rd col, and NE tag.
featuresets = [ (ne_features1((val[0], val[1])), val[3]) for val in trainTokens]

Task1Classifier = nltk.NaiveBayesClassifier.train(featuresets)

 
#Uncomment to rewrite the output tag files
writer(Task1Classifier, 'eng.dev', 'out1_dev.txt')
writer(Task1Classifier, 'eng.test', 'out1_test.txt')
 
displayAccuracy("1")
# # Done with Task 1, fill in some code above, which evaluates the Task1 Classifier.
