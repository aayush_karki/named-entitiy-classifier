#Qasim Nadeem, Aayush Karki

import nltk, re, math


from genericFunctions import displayAccuracy



# Features for NER classifier  in Task 2. Uses lexical context to assign features to a word. This function
# takes in a sentence and the index for the word in the sentence that it needs to return the features for.
def ne_features2(sentence,i):
    word = sentence[i][0]
    posTag = sentence[i][1]
 
    features = {"prefix(4)": word.lower()[:4],"suffix(2)": word.lower()[-2:],
                "suffix(5)": word.lower()[-5:], "posTag": posTag}
 
 
  #add the word's POS tag as a feature if word is a proper noun else add empty string
    if posTag=='NNP' or posTag=='NNPS':
        features['word']= word
    else:
        features['word'] = ''

#add True as feature if the prev word was a DT 
    features['prevIsDT'] = ((i!=0) and sentence[i-1][1]=='DT')
 
    #add previous word's POS tag as a feature.
    if i == 0:
        features["prevPOStag"] = "<START>"
    else:
        features["prevPOStag"] = sentence[i-1][1]

    # Getting the POS tags of word 2-words earlier and later, than the target word: 
    if i<=1:
        features["2WordsEarlierPOS"] = "<START>"
    else:
        features["2WordsEarlierPOS"] = sentence[i-2][1]

    if i >= (len(sentence)- 2):
        features["2WordsAfterPOS"] = "<END>"
    else:
        features["2WordsAfterPOS"] = sentence[i+2][1]
 
    #add next word's POS tag as a feature.
    if i == (len(sentence)-1):
        features["nextPOStag"] = "<END>"
    else:
        features["nextPOStag"] = sentence[i+1][1]
 
    return features


 
###############################
# Slightly different from writer() for Task 1; does the same job, but slightly different requirements
# for Task 2, that's why this is needed.
# Takes input file, classifies its tokens using the given classifier
# as argument, and writes to outputfile in correct format for evaluation later.
def writer2(T2Classifier, inpfile, outfile):
    g=open(outfile,'w')
     
    Sents = [[]]
 
    with open(inpfile,'r') as File:# getting dev/test file.
            for line in File:
                vals = line.split() #should be 4 values on a line, or emtpy list if empty line.
                 
                if vals == []: #whitespace on line. New sentence starting.
                    Sents.append([])
                else:
                    Sents[-1].append(tuple(vals))
    File.close()
     
    for sent in Sents:
        for i in range(len(sent)):
            word = sent[i][0]
            goldTag = sent[i][3]
            wordFeatures = ne_features2([(val[0],val[1],val[2]) for val in sent], i)
 
            predTag = T2Classifier.classify(wordFeatures)
 
            if predTag !='O': 
                predTag= ("I-"+ predTag[2:]) #changing B-x to I-x.
                 
            wline = word + " " + goldTag +  " " + predTag
            g.write(wline+'\n')
    g.close()
 


trainSents = [[]] # A list of lists. Each sublist represents a sentence in trainFile, and has a series of tuples of form
                   # (word, POS tag, 3rd col, NE tag).
 
with open('eng.train','r') as trainFile:# getting train file.
        for line in trainFile:
            vals = line.split() #should be 4 values on a line, or emtpy list if empty line.
             
            if vals == []: #whitespace on line. New sentence starting.
                trainSents.append([])
            else:
                trainSents[-1].append(tuple(vals))         
trainFile.close()
 
#I now have entire trainSents as a list of lists (each sublist consists of tuples).
featuresets = []
 
for tagged_sent in trainSents: # iterating over the lists in trainSents.
    for i in range(len(tagged_sent)):
        wordFeatures = ne_features2([(val[0],val[1],val[2]) for val in tagged_sent], i) #features of the word at position i.
        featuresets.append((wordFeatures,  tagged_sent[i][3])) # append word features, and it's NE tag as a tuple
         
Task2Classifier = nltk.NaiveBayesClassifier.train(featuresets)
 
#Uncomment to rewrite the output tag files
writer2(Task2Classifier, 'eng.dev', 'out2_dev.txt')
writer2(Task2Classifier, 'eng.test', 'out2_test.txt')
 
displayAccuracy("2")


##########DONE WITH TASK 2.