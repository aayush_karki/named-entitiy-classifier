#Qasim Nadeem, Aayush Karki

#Running this file prints accuracy for all 4 tasks



print "Processing Task 1 of 4: Will take less than 10-15 seconds..."
from finalTask1 import displayAccuracy 
print "\n ------------------------------------------------------ \n"

print "Processing Task 2 of 4: Will take less than 15-20 seconds..."
from finalTask2 import displayAccuracy 
print "\n ------------------------------------------------------ \n"

print "Processing Task 3 of 4: Will take less than 20-25 seconds..."
from finalTask3 import displayAccuracy 
print "\n ------------------------------------------------------ \n"

print "Processing Task 4 of 4: Will take less than ~30 seconds..."
from finalTask4 import displayAccuracy 
print "\n ------------------------------------------------------ \n"

print "\n All four task processed. Finished."