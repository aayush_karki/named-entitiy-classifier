This is a final project for 15-383 Intro to Text Processing at Carnegie Mellon by Qasim Nadeem and Aayush Karki.

It uses training set / development set to train a classifier. The classifier categorizes (English) words
 into person, place, organization, and otherwise.

 There are four implementations for the above task for this project .

 To run, for eg finalTast1.py, simply execute: python finalTask1.py 

 To run all four task, execute: python runAllFourTasks.py . This will take 1-2 minutes to run to completion.


