#Qasim Nadeem, Aayush Karki


# GENERIC evaluate function. Takes in a file that is in the correct format for
# evalutation, meaning every row has (word, Gold Std tag, Predicted tag)
# Also takes in the label (PER or ORG or LOC or IvsO(overall) etc) that is to be evaluated.
# Returns the tuple (percision, recall, F1 measure).
def evaluate_label(filename , label='I'):
    TP,FP,FN,TN= 0,0,0,0


    f = open(filename,'r')
    for line in f:
        words = line.split()
        (g,p) =  words[1], words[2]
        
        if label=='I':
            g=g[0]
            p=p[0]
        else:
            g=g[2:]
            p=p[2:]
            
        if (g==label and p==label):
            TP+=1
        elif (g!=label and p==label ):
            FP+=1
        elif (g==label and p!=label):
            FN+=1
        else:
            TN+=1

    f.close() 
    precision = (1.0*TP)/(TP+FP)
    recall = (1.0*TP)/(TP+FN)
    FMeasure = (2*precision*recall)/(precision+recall)
    
    return (round(100*precision,1), 
            round(100*recall,1), 
            round(100*FMeasure,1))
    


    
    
def displayAccuracy(fileNumber_str):
    print "TASK %s evaluation :\n" %fileNumber_str

    print "For test set \n"
    outputfiletest = 'out%s_test.txt' %fileNumber_str
    
    orgAcc = evaluate_label(outputfiletest,'ORG')
    locAcc = evaluate_label(outputfiletest,'LOC')
    perAcc = evaluate_label(outputfiletest,'PER')
    IvOAcc = evaluate_label(outputfiletest)

    print('{0}   {1}   {2}   {3}'.format("Label", "Precision%",
                                         "Recall%","F-Measure%") )  
    tableSpacing = '{0}      {1}       {2}      {3}' 
    print(tableSpacing.format("ORG  ",orgAcc[0],orgAcc[1],orgAcc[2]))
    print(tableSpacing.format("LOC  ",locAcc[0],locAcc[1],locAcc[2]))
    print(tableSpacing.format("PER  ",perAcc[0],perAcc[1],perAcc[2]))
    print(tableSpacing.format("I v O",IvOAcc[0],IvOAcc[1],IvOAcc[2]))


    print "\n\n For Dev set \n"
    outputfileDev = 'out%s_dev.txt' %fileNumber_str
     
    orgAcc = evaluate_label(outputfileDev,'ORG')
    locAcc = evaluate_label(outputfileDev,'LOC')
    perAcc = evaluate_label(outputfileDev,'PER')
    IvOAcc = evaluate_label(outputfileDev)
 
    print('{0}   {1}   {2}   {3}'.format("Label", "Precision%",
                                         "Recall%","F-Measure%") )  
    print(tableSpacing.format("ORG  ",orgAcc[0],orgAcc[1],orgAcc[2]))
    print(tableSpacing.format("LOC  ",locAcc[0],locAcc[1],locAcc[2]))
    print(tableSpacing.format("PER  ",perAcc[0],perAcc[1],perAcc[2]))
    print(tableSpacing.format("I v O",IvOAcc[0],IvOAcc[1],IvOAcc[2]))
    print
    
    return    